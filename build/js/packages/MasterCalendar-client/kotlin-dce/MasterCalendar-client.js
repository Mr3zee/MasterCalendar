(function (root, factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', 'kotlin', 'kotlinx-html-js', 'kotlinx-coroutines-core', 'kotlinx-serialization-kotlinx-serialization-core-jsLegacy', 'kotlinx-serialization-kotlinx-serialization-json-jsLegacy'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('kotlin'), require('kotlinx-html-js'), require('kotlinx-coroutines-core'), require('kotlinx-serialization-kotlinx-serialization-core-jsLegacy'), require('kotlinx-serialization-kotlinx-serialization-json-jsLegacy'));
  else {
    if (typeof kotlin === 'undefined') {
      throw new Error("Error loading module 'MasterCalendar-client'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'MasterCalendar-client'.");
    }if (typeof this['kotlinx-html-js'] === 'undefined') {
      throw new Error("Error loading module 'MasterCalendar-client'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'MasterCalendar-client'.");
    }if (typeof this['kotlinx-coroutines-core'] === 'undefined') {
      throw new Error("Error loading module 'MasterCalendar-client'. Its dependency 'kotlinx-coroutines-core' was not found. Please, check whether 'kotlinx-coroutines-core' is loaded prior to 'MasterCalendar-client'.");
    }if (typeof this['kotlinx-serialization-kotlinx-serialization-core-jsLegacy'] === 'undefined') {
      throw new Error("Error loading module 'MasterCalendar-client'. Its dependency 'kotlinx-serialization-kotlinx-serialization-core-jsLegacy' was not found. Please, check whether 'kotlinx-serialization-kotlinx-serialization-core-jsLegacy' is loaded prior to 'MasterCalendar-client'.");
    }if (typeof this['kotlinx-serialization-kotlinx-serialization-json-jsLegacy'] === 'undefined') {
      throw new Error("Error loading module 'MasterCalendar-client'. Its dependency 'kotlinx-serialization-kotlinx-serialization-json-jsLegacy' was not found. Please, check whether 'kotlinx-serialization-kotlinx-serialization-json-jsLegacy' is loaded prior to 'MasterCalendar-client'.");
    }root['MasterCalendar-client'] = factory(typeof this['MasterCalendar-client'] === 'undefined' ? {} : this['MasterCalendar-client'], kotlin, this['kotlinx-html-js'], this['kotlinx-coroutines-core'], this['kotlinx-serialization-kotlinx-serialization-core-jsLegacy'], this['kotlinx-serialization-kotlinx-serialization-json-jsLegacy']);
  }
}(this, function (_, Kotlin, $module$kotlinx_html_js, $module$kotlinx_coroutines_core, $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy, $module$kotlinx_serialization_kotlinx_serialization_json_jsLegacy) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var split = Kotlin.kotlin.text.split_ip8yn$;
  var capitalize = Kotlin.kotlin.text.capitalize_pdl1vz$;
  var joinToString = Kotlin.kotlin.collections.joinToString_fmv235$;
  var collectionSizeOrDefault = Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
  var checkIndexOverflow = Kotlin.kotlin.collections.checkIndexOverflow_za3lpa$;
  var Unit = Kotlin.kotlin.Unit;
  var ensureNotNull = Kotlin.ensureNotNull;
  var throwCCE = Kotlin.throwCCE;
  var to = Kotlin.kotlin.to_ujzrz7$;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var set_id = $module$kotlinx_html_js.kotlinx.html.set_id_ueiko3$;
  var hashMapOf = Kotlin.kotlin.collections.hashMapOf_qfcya0$;
  var equals = Kotlin.equals;
  var removeClass = Kotlin.kotlinx.dom.removeClass_hhb33f$;
  var addClass = Kotlin.kotlinx.dom.addClass_hhb33f$;
  var attributesMapOf = $module$kotlinx_html_js.kotlinx.html.attributesMapOf_jyasbz$;
  var SPAN_init = $module$kotlinx_html_js.kotlinx.html.SPAN;
  var visitTag = $module$kotlinx_html_js.kotlinx.html.visitTag_xwv8ym$;
  var DIV_init = $module$kotlinx_html_js.kotlinx.html.DIV;
  var visitTagAndFinalize = $module$kotlinx_html_js.kotlinx.html.visitTagAndFinalize_g9qte5$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var InputType = $module$kotlinx_html_js.kotlinx.html.InputType;
  var set_style = $module$kotlinx_html_js.kotlinx.html.set_style_ueiko3$;
  var json = Kotlin.kotlin.js.json_pyyo18$;
  var coroutines = $module$kotlinx_coroutines_core.kotlinx.coroutines;
  var await_0 = $module$kotlinx_coroutines_core.kotlinx.coroutines.await_t11jrl$;
  var COROUTINE_SUSPENDED = Kotlin.kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED;
  var CoroutineImpl = Kotlin.kotlin.coroutines.CoroutineImpl;
  var promise = $module$kotlinx_coroutines_core.kotlinx.coroutines.promise_pda6u4$;
  var dropLast = Kotlin.kotlin.text.dropLast_6ic1pp$;
  var toInt = Kotlin.kotlin.text.toInt_pdl1vz$;
  var NumberFormatException = Kotlin.kotlin.NumberFormatException;
  var listOf = Kotlin.kotlin.collections.listOf_mh5how$;
  var kotlin_js_internal_StringCompanionObject = Kotlin.kotlin.js.internal.StringCompanionObject;
  var serializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.builtins.serializer_6eet4j$;
  var ListSerializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.builtins.ListSerializer_swdriu$;
  var PrimitiveClasses$stringClass = Kotlin.kotlin.reflect.js.internal.PrimitiveClasses.stringClass;
  var createKType = Kotlin.createKType;
  var createInvariantKTypeProjection = Kotlin.createInvariantKTypeProjection;
  var MapSerializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.builtins.MapSerializer_2yqygg$;
  var toString = Kotlin.toString;
  var Json = $module$kotlinx_serialization_kotlinx_serialization_json_jsLegacy.kotlinx.serialization.json.Json;
  var PrimitiveClasses$arrayClass = Kotlin.kotlin.reflect.js.internal.PrimitiveClasses.arrayClass;
  var enumEncode = $module$kotlinx_html_js.kotlinx.html.attributes.enumEncode_m4whry$;
  var attributesMapOf_0 = $module$kotlinx_html_js.kotlinx.html.attributesMapOf_alerag$;
  var INPUT_init = $module$kotlinx_html_js.kotlinx.html.INPUT;
  var HTMLDivElement_0 = HTMLDivElement;
  var serializer_0 = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.serializer_ca95z9$;
  var KSerializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.KSerializer;
  var HashMap_init = Kotlin.kotlin.collections.HashMap_init_q3lmfv$;
  var L1000 = Kotlin.Long.fromInt(1000);
  var delay = $module$kotlinx_coroutines_core.kotlinx.coroutines.delay_s8cxhz$;
  var last = Kotlin.kotlin.collections.last_2p1efm$;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var IllegalArgumentException_init = Kotlin.kotlin.IllegalArgumentException_init_pdl1vj$;
  var LABEL_init = $module$kotlinx_html_js.kotlinx.html.LABEL;
  var getKClass = Kotlin.getKClass;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var PluginGeneratedSerialDescriptor = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.internal.PluginGeneratedSerialDescriptor;
  var internal = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.internal;
  var LinkedHashMapSerializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.internal.LinkedHashMapSerializer;
  var UnknownFieldException = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.UnknownFieldException;
  var GeneratedSerializer = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.internal.GeneratedSerializer;
  var MissingFieldException = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.MissingFieldException;
  var SerializerFactory = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy.kotlinx.serialization.internal.SerializerFactory;
  var defineInlineFunction = Kotlin.defineInlineFunction;
  var wrapFunction = Kotlin.wrapFunction;
  var LinkedHashMap_init = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$;
  function main() {
    onloadPageByName(window.location.pathname);
  }
  function onloadPageByName(pageName) {
    switch (getPageJSFileName(pageName)) {
      case '':
        mainPage();
        break;
      case 'login':
        loginPage();
        break;
      case 'register':
        registerPage();
        break;
      case 'settings':
        settingsPage();
        break;
      default:notFoundPage();
        break;
    }
  }
  function getPageJSFileName(path) {
    var $receiver = split(path, ['/']);
    var destination = ArrayList_init(collectionSizeOrDefault($receiver, 10));
    var tmp$, tmp$_0;
    var index = 0;
    tmp$ = $receiver.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      destination.add_11rb$(checkIndexOverflow((tmp$_0 = index, index = tmp$_0 + 1 | 0, tmp$_0)) === 1 ? item : capitalize(item));
    }
    return joinToString(destination, '');
  }
  function visit$lambda(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_0(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visitAndFinalize$lambda(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function loginPage$lambda(it) {
    sendAuthRequest('login', ['log-input1', 'log-input2']);
    addInputEvents();
    return Unit;
  }
  function loginPage() {
    window.onload = loginPage$lambda;
  }
  function sendAuthRequest$lambda(it) {
    window.location.replace('/');
    return Unit;
  }
  function sendAuthRequest$lambda_0(closure$type) {
    return function (it) {
      addErrorMessage(it, closure$type);
      return Unit;
    };
  }
  function sendAuthRequest(type, inputs) {
    setRequestEvent('/auth', 'button', type, inputs, sendAuthRequest$lambda, sendAuthRequest$lambda_0(type));
  }
  function addErrorMessage$lambda(closure$response, closure$errorHeader, closure$message, closure$errorText, closure$errorBlock) {
    return function (it) {
      closure$errorHeader.innerHTML = capitalize(closure$response);
      closure$errorText.innerHTML = closure$message;
      closure$errorBlock.style.setProperty('--error-text-opacity', '1');
      return Unit;
    };
  }
  function addErrorMessage$lambda_0() {
    var o = {};
    o['passive'] = false;
    o['once'] = false;
    o['capture'] = false;
    return to(o.once, true);
  }
  function addErrorMessage(response, type) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    var message = ensureNotNull((tmp$ = map.get_11rb$(type)) != null ? tmp$(response) : null);
    if (document.getElementById('error-block') == null) {
      var body = Kotlin.isType(tmp$_0 = document.getElementsByClassName('body-class')[0], HTMLDivElement) ? tmp$_0 : throwCCE();
      body.style.marginLeft = 'calc(50% - (var(--block-width-' + type + ') + var(--error-block-offset) + var(--error-block-width)) / 2)';
      body.after(createErrorBlock(capitalize(response), message));
      var errorBlock = Kotlin.isType(tmp$_1 = ensureNotNull(document.getElementById('error-block')), HTMLDivElement) ? tmp$_1 : throwCCE();
      errorBlock.getBoundingClientRect();
      errorBlock.style.setProperty('--error-opacity', '1');
      errorBlock.style.setProperty('transform', 'scale(1.02)');
    } else {
      var errorBlock_0 = Kotlin.isType(tmp$_2 = ensureNotNull(document.getElementById('error-block')), HTMLDivElement) ? tmp$_2 : throwCCE();
      var errorHeader = Kotlin.isType(tmp$_3 = ensureNotNull(document.getElementById('error-name')), HTMLSpanElement) ? tmp$_3 : throwCCE();
      var errorText = Kotlin.isType(tmp$_4 = ensureNotNull(document.getElementById('error-msg')), HTMLSpanElement) ? tmp$_4 : throwCCE();
      errorHeader.style.setProperty('opacity', 'var(--error-text-opacity)');
      errorText.style.setProperty('opacity', 'var(--error-text-opacity)');
      errorHeader.style.setProperty('transition', 'opacity .2s linear');
      errorText.style.setProperty('transition', 'opacity .2s linear');
      errorBlock_0.style.setProperty('--error-text-opacity', '0');
      errorHeader.addEventListener('transitionend', addErrorMessage$lambda(response, errorHeader, message, errorText, errorBlock_0), addErrorMessage$lambda_0);
    }
  }
  function createErrorBlock$lambda$lambda$lambda($receiver) {
    set_id($receiver, 'error-header');
    $receiver.unaryPlus_pdl1vz$('Error:');
    return Unit;
  }
  function createErrorBlock$lambda$lambda$lambda_0(closure$header) {
    return function ($receiver) {
      set_id($receiver, 'error-name');
      $receiver.unaryPlus_pdl1vz$(closure$header);
      return Unit;
    };
  }
  function createErrorBlock$lambda$lambda(closure$header) {
    return function ($receiver) {
      visitTag(new SPAN_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda(createErrorBlock$lambda$lambda$lambda));
      var block = createErrorBlock$lambda$lambda$lambda_0(closure$header);
      visitTag(new SPAN_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda(block));
      return Unit;
    };
  }
  function createErrorBlock$lambda$lambda$lambda_1(closure$message) {
    return function ($receiver) {
      set_id($receiver, 'error-msg');
      $receiver.unaryPlus_pdl1vz$(closure$message);
      return Unit;
    };
  }
  function createErrorBlock$lambda$lambda_0(closure$message) {
    return function ($receiver) {
      var block = createErrorBlock$lambda$lambda$lambda_1(closure$message);
      visitTag(new SPAN_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda(block));
      return Unit;
    };
  }
  function createErrorBlock$lambda(closure$header, closure$message) {
    return function ($receiver) {
      set_id($receiver, 'error-block');
      var classes = 'error-header';
      var block = createErrorBlock$lambda$lambda(closure$header);
      visitTag(new DIV_init(attributesMapOf('class', classes), $receiver.consumer), visit$lambda_0(block));
      var classes_0 = 'error-body';
      var block_0 = createErrorBlock$lambda$lambda_0(closure$message);
      visitTag(new DIV_init(attributesMapOf('class', classes_0), $receiver.consumer), visit$lambda_0(block_0));
      return Unit;
    };
  }
  function createErrorBlock(header, message) {
    var $receiver = get_create(document);
    return visitTagAndFinalize(new DIV_init(attributesMapOf('class', null), $receiver), $receiver, visitAndFinalize$lambda(createErrorBlock$lambda(header, message)));
  }
  function map$lambda(it) {
    return 'Invalid login or password. Please check your credentials and try again or try to recover your password';
  }
  function map$lambda_0(it) {
    switch (it) {
      case 'name':
        return 'This name is already taken. Please, choose another one, as every name should by unique';
      case 'email':
        return 'Invalid email syntax. Please, check your input, it should look like example@mail.com';
      case 'password':
        return 'Password should be at least 12 symbols, ' + 'containing lower and upper case letters, digits and special characters: @\\$!%*?&';
      case 'repeat-password':
        return 'Passwords do not match. Please, type in correct password and try again';
      default:return 'Unknown error occurred';
    }
  }
  var map;
  function addInputEvents$lambda(closure$labels, closure$i, closure$input) {
    return function (it) {
      var tmp$;
      var label = Kotlin.isType(tmp$ = ensureNotNull(closure$labels.item(closure$i)), HTMLLabelElement) ? tmp$ : throwCCE();
      if (equals(closure$input.value, '')) {
        removeClass(label, ['above']);
        addClass(label, ['inside']);
      }return Unit;
    };
  }
  function addInputEvents$lambda_0(closure$labels, closure$i) {
    return function (it) {
      var tmp$;
      var label = Kotlin.isType(tmp$ = ensureNotNull(closure$labels.item(closure$i)), HTMLLabelElement) ? tmp$ : throwCCE();
      removeClass(label, ['inside']);
      addClass(label, ['above']);
      return Unit;
    };
  }
  function addInputEvents() {
    var tmp$, tmp$_0;
    var elements = document.getElementsByClassName('form-input');
    var labels = document.getElementsByClassName('form-label');
    tmp$ = elements.length;
    for (var i = 0; i < tmp$; i++) {
      var input = Kotlin.isType(tmp$_0 = ensureNotNull(elements.item(i)), HTMLInputElement) ? tmp$_0 : throwCCE();
      input.addEventListener('focusout', addInputEvents$lambda(labels, i, input));
      input.addEventListener('focus', addInputEvents$lambda_0(labels, i));
    }
  }
  function visit$lambda_1(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_2(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_3(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function div$lambda_1($receiver) {
    return Unit;
  }
  function visitAndFinalize$lambda_0(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function mainPage$lambda(it) {
    setHideShowButton();
    requestTables();
    requestName();
    return Unit;
  }
  function mainPage() {
    window.onload = mainPage$lambda;
  }
  function headerHeight() {
    return window.getComputedStyle(root).getPropertyValue('--header-height');
  }
  function setHideShowButton$lambda(closure$button) {
    return function (it) {
      var hidden = equals(closure$button.style.getPropertyValue('--button-hidden'), 'true');
      hideInfoBar(hidden);
      expandMainBlock(hidden);
      if (!hidden)
        changeButtonText(closure$button, 'Show Sidebar');
      else
        changeButtonText(closure$button, 'Hide Sidebar');
      closure$button.style.setProperty('--button-hidden', (!hidden).toString());
      return Unit;
    };
  }
  function setHideShowButton() {
    var tmp$;
    var button = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('hide-show-button')), HTMLInputElement) ? tmp$ : throwCCE();
    button.onclick = setHideShowButton$lambda(button);
  }
  function expandMainBlock$lambda(closure$hidden) {
    return function ($receiver) {
      $receiver.style.width = !closure$hidden ? '100%' : 'calc(100% - var(--sidebar-width) - var(--sidebar-offset))';
      return Unit;
    };
  }
  function expandMainBlock(hidden) {
    var tmp$;
    expandMainBlock$lambda(hidden)(Kotlin.isType(tmp$ = document.getElementById('main'), HTMLDivElement) ? tmp$ : throwCCE());
  }
  function hideInfoBar$hiddenInfoBarWidth(style) {
    return 'calc(' + headerHeight() + ' - ' + style.getPropertyValue('height') + ' + var(--button-height))';
  }
  function hideInfoBar$lambda(closure$hidden, closure$hiddenInfoBarWidth) {
    return function ($receiver) {
      $receiver.style.top = closure$hidden ? headerHeight() : closure$hiddenInfoBarWidth(window.getComputedStyle($receiver));
      return Unit;
    };
  }
  function hideInfoBar(hidden) {
    var tmp$;
    var hiddenInfoBarWidth = hideInfoBar$hiddenInfoBarWidth;
    hideInfoBar$lambda(hidden, hiddenInfoBarWidth)(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('info-bar')), HTMLDivElement) ? tmp$ : throwCCE());
  }
  function changeButtonText$lambda(closure$newText) {
    return function (it) {
      return buttonChange(it, closure$newText);
    };
  }
  function changeButtonText(button, newText) {
    blockButton(button, changeButtonText$lambda(newText));
  }
  function Table(name, months) {
    this.name = name;
    this.months = months;
  }
  Table.$metadata$ = {kind: Kind_CLASS, simpleName: 'Table', interfaces: []};
  Table.prototype.component1 = function () {
    return this.name;
  };
  Table.prototype.component2 = function () {
    return this.months;
  };
  Table.prototype.copy_346odf$ = function (name, months) {
    return new Table(name === void 0 ? this.name : name, months === void 0 ? this.months : months);
  };
  Table.prototype.toString = function () {
    return 'Table(name=' + Kotlin.toString(this.name) + (', months=' + Kotlin.toString(this.months)) + ')';
  };
  Table.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    result = result * 31 + Kotlin.hashCode(this.months) | 0;
    return result;
  };
  Table.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.name, other.name) && Kotlin.equals(this.months, other.months)))));
  };
  function Month(date, days) {
    this.date = date;
    this.days = days;
  }
  Month.$metadata$ = {kind: Kind_CLASS, simpleName: 'Month', interfaces: []};
  Month.prototype.component1 = function () {
    return this.date;
  };
  Month.prototype.component2 = function () {
    return this.days;
  };
  Month.prototype.copy_7aq49u$ = function (date, days) {
    return new Month(date === void 0 ? this.date : date, days === void 0 ? this.days : days);
  };
  Month.prototype.toString = function () {
    return 'Month(date=' + Kotlin.toString(this.date) + (', days=' + Kotlin.toString(this.days)) + ')';
  };
  Month.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.date) | 0;
    result = result * 31 + Kotlin.hashCode(this.days) | 0;
    return result;
  };
  Month.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.date, other.date) && Kotlin.equals(this.days, other.days)))));
  };
  function Day(index, chosen) {
    this.index = index;
    this.chosen = chosen;
  }
  Day.$metadata$ = {kind: Kind_CLASS, simpleName: 'Day', interfaces: []};
  Day.prototype.component1 = function () {
    return this.index;
  };
  Day.prototype.component2 = function () {
    return this.chosen;
  };
  Day.prototype.copy_fzusl$ = function (index, chosen) {
    return new Day(index === void 0 ? this.index : index, chosen === void 0 ? this.chosen : chosen);
  };
  Day.prototype.toString = function () {
    return 'Day(index=' + Kotlin.toString(this.index) + (', chosen=' + Kotlin.toString(this.chosen)) + ')';
  };
  Day.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.index) | 0;
    result = result * 31 + Kotlin.hashCode(this.chosen) | 0;
    return result;
  };
  Day.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.index, other.index) && Kotlin.equals(this.chosen, other.chosen)))));
  };
  function NormalDate(year, month, day) {
    this.year = year;
    this.month = month;
    this.day = day;
  }
  NormalDate.prototype.toString = function () {
    return this.getDay() + '/' + this.getMonth() + '/' + this.year;
  };
  NormalDate.prototype.getMonth = function () {
    return this.month < 10 ? '0' + this.month : this.month.toString();
  };
  NormalDate.prototype.getDay = function () {
    return this.day < 10 ? '0' + this.day : this.day.toString();
  };
  NormalDate.prototype.getShortMonth = function () {
    var tmp$;
    switch (this.month) {
      case 0:
        tmp$ = 'Jan';
        break;
      case 1:
        tmp$ = 'Feb';
        break;
      case 2:
        tmp$ = 'Mar';
        break;
      case 3:
        tmp$ = 'Apr';
        break;
      case 4:
        tmp$ = 'May';
        break;
      case 5:
        tmp$ = 'Jun';
        break;
      case 6:
        tmp$ = 'Jul';
        break;
      case 7:
        tmp$ = 'Aug';
        break;
      case 8:
        tmp$ = 'Sep';
        break;
      case 9:
        tmp$ = 'Oct';
        break;
      case 10:
        tmp$ = 'Nov';
        break;
      default:tmp$ = 'Dec';
        break;
    }
    return tmp$;
  };
  NormalDate.$metadata$ = {kind: Kind_CLASS, simpleName: 'NormalDate', interfaces: []};
  NormalDate.prototype.component1 = function () {
    return this.year;
  };
  NormalDate.prototype.component2 = function () {
    return this.month;
  };
  NormalDate.prototype.component3 = function () {
    return this.day;
  };
  NormalDate.prototype.copy_qt1dr2$ = function (year, month, day) {
    return new NormalDate(year === void 0 ? this.year : year, month === void 0 ? this.month : month, day === void 0 ? this.day : day);
  };
  NormalDate.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.year) | 0;
    result = result * 31 + Kotlin.hashCode(this.month) | 0;
    result = result * 31 + Kotlin.hashCode(this.day) | 0;
    return result;
  };
  NormalDate.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.year, other.year) && Kotlin.equals(this.month, other.month) && Kotlin.equals(this.day, other.day)))));
  };
  function addTables(tables) {
    var tmp$, tmp$_0;
    var container = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('main-container')), HTMLDivElement) ? tmp$ : throwCCE();
    tmp$_0 = tables.iterator();
    while (tmp$_0.hasNext()) {
      var table = tmp$_0.next();
      addTable(container, table);
      var $receiver = tablesSizes;
      var key = table.name;
      var value = table.months.size;
      $receiver.put_xwzc9p$(key, value);
      var $receiver_0 = tableMoving;
      var key_0 = table.name;
      $receiver_0.put_xwzc9p$(key_0, false);
    }
  }
  function addTable(container, table) {
    container.appendChild(createTable(table));
    addOnclickDayEvent(table.name);
    addOnclickArrowEvents(table.name);
  }
  function createTable$lambda$lambda$lambda$lambda(closure$name) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$name);
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda(closure$name) {
    return function ($receiver) {
      var block = createTable$lambda$lambda$lambda$lambda(closure$name);
      visitTag(new SPAN_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda_1(block));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda_0(closure$name, closure$defDate) {
    return function ($receiver) {
      set_id($receiver, closure$name + '-cell-date');
      $receiver.unaryPlus_pdl1vz$(getMonth(closure$defDate));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda_0(closure$name, closure$defDate) {
    return function ($receiver) {
      var block = createTable$lambda$lambda$lambda$lambda_0(closure$name, closure$defDate);
      visitTag(new SPAN_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda_1(block));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda$lambda(closure$name) {
    return function ($receiver) {
      set_id($receiver, closure$name + '-up-arrow');
      $receiver.type = InputType.button;
      $receiver.value = 'Up';
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda$lambda_0(closure$name) {
    return function ($receiver) {
      set_id($receiver, closure$name + '-down-arrow');
      $receiver.type = InputType.button;
      $receiver.value = 'Down';
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda_1(closure$name) {
    return function ($receiver) {
      var classes = 'arrow ' + closure$name + '-arrow';
      var block = createTable$lambda$lambda$lambda$lambda$lambda(closure$name);
      visitTag(new INPUT_init(attributesMapOf_0(['type', null != null ? enumEncode(null) : null, 'formenctype', null != null ? enumEncode(null) : null, 'formmethod', null != null ? enumEncode(null) : null, 'name', null, 'class', classes]), $receiver.consumer), visit$lambda_2(block));
      var classes_0 = 'arrow ' + closure$name + '-arrow';
      var block_0 = createTable$lambda$lambda$lambda$lambda$lambda_0(closure$name);
      visitTag(new INPUT_init(attributesMapOf_0(['type', null != null ? enumEncode(null) : null, 'formenctype', null != null ? enumEncode(null) : null, 'formmethod', null != null ? enumEncode(null) : null, 'name', null, 'class', classes_0]), $receiver.consumer), visit$lambda_2(block_0));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda_1(closure$name) {
    return function ($receiver) {
      var classes = 'cell-container';
      var block = createTable$lambda$lambda$lambda$lambda_1(closure$name);
      visitTag(new DIV_init(attributesMapOf('class', classes), $receiver.consumer), visit$lambda_3(block));
      return Unit;
    };
  }
  function createTable$lambda$lambda(closure$name, closure$defDate) {
    return function ($receiver) {
      var classes = 'cell-name';
      var block = createTable$lambda$lambda$lambda(closure$name);
      visitTag(new DIV_init(attributesMapOf('class', classes), $receiver.consumer), visit$lambda_3(block));
      visitTag(new DIV_init(attributesMapOf('class', 'control-border'), $receiver.consumer), visit$lambda_3(div$lambda_1));
      var classes_0 = 'cell-date';
      var block_0 = createTable$lambda$lambda$lambda_0(closure$name, closure$defDate);
      visitTag(new DIV_init(attributesMapOf('class', classes_0), $receiver.consumer), visit$lambda_3(block_0));
      visitTag(new DIV_init(attributesMapOf('class', 'control-border'), $receiver.consumer), visit$lambda_3(div$lambda_1));
      var classes_1 = 'cell-arrows';
      var block_1 = createTable$lambda$lambda$lambda_1(closure$name);
      visitTag(new DIV_init(attributesMapOf('class', classes_1), $receiver.consumer), visit$lambda_3(block_1));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda$lambda$lambda$lambda(closure$month, closure$day) {
    return function ($receiver) {
      set_id($receiver, (new NormalDate(closure$month.date.year, closure$month.date.month, closure$day.index)).toString());
      $receiver.type = InputType.button;
      $receiver.value = closure$day.index.toString();
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda$lambda$lambda(closure$color, closure$name, closure$month, closure$day) {
    return function ($receiver) {
      set_style($receiver, 'background: ' + closure$color + ';');
      var classes = 'day-button ' + closure$name + '-day-button';
      var block = createTable$lambda$lambda$lambda$lambda$lambda$lambda$lambda(closure$month, closure$day);
      visitTag(new INPUT_init(attributesMapOf_0(['type', null != null ? enumEncode(null) : null, 'formenctype', null != null ? enumEncode(null) : null, 'formmethod', null != null ? enumEncode(null) : null, 'name', null, 'class', classes]), $receiver.consumer), visit$lambda_2(block));
      return Unit;
    };
  }
  function createTable$lambda$lambda$lambda$lambda_2(closure$name, closure$month) {
    return function ($receiver) {
      set_id($receiver, closure$name + '-' + closure$month.date.getMonth() + '-' + closure$month.date.year + '-month');
      var offset = makeDaysOffset($receiver, closure$month.date);
      var $receiver_0 = closure$month.days;
      var destination = ArrayList_init(collectionSizeOrDefault($receiver_0, 10));
      var tmp$;
      tmp$ = $receiver_0.iterator();
      while (tmp$.hasNext()) {
        var item = tmp$.next();
        var tmp$_0 = destination.add_11rb$;
        var color = item.chosen ? secondColor() : mainColor();
        var block = createTable$lambda$lambda$lambda$lambda$lambda$lambda(color, closure$name, closure$month, item);
        visitTag(new DIV_init(attributesMapOf('class', 'day'), $receiver.consumer), visit$lambda_3(block));
        tmp$_0.call(destination, Unit);
      }
      addBlank($receiver, 42 - (offset + closure$month.days.size) | 0);
      return Unit;
    };
  }
  function createTable$lambda$lambda_0(closure$name, closure$table) {
    return function ($receiver) {
      set_id($receiver, closure$name + '-month-feed');
      var $receiver_0 = closure$table.months;
      var destination = ArrayList_init(collectionSizeOrDefault($receiver_0, 10));
      var tmp$;
      tmp$ = $receiver_0.iterator();
      while (tmp$.hasNext()) {
        var item = tmp$.next();
        var tmp$_0 = destination.add_11rb$;
        var block = createTable$lambda$lambda$lambda$lambda_2(closure$name, item);
        visitTag(new DIV_init(attributesMapOf('class', 'month'), $receiver.consumer), visit$lambda_3(block));
        tmp$_0.call(destination, Unit);
      }
      return Unit;
    };
  }
  function createTable$lambda(closure$name, closure$defDate, closure$table) {
    return function ($receiver) {
      set_id($receiver, 'cell-' + closure$name);
      var classes = 'cell-control';
      var block = createTable$lambda$lambda(closure$name, closure$defDate);
      visitTag(new DIV_init(attributesMapOf('class', classes), $receiver.consumer), visit$lambda_3(block));
      var classes_0 = 'months-feed';
      var block_0 = createTable$lambda$lambda_0(closure$name, closure$table);
      visitTag(new DIV_init(attributesMapOf('class', classes_0), $receiver.consumer), visit$lambda_3(block_0));
      return Unit;
    };
  }
  function createTable(table) {
    var name = table.name;
    var defDate = table.months.get_za3lpa$(0).date;
    var $receiver = get_create(document);
    var tmp$;
    return Kotlin.isType(tmp$ = visitTagAndFinalize(new DIV_init(attributesMapOf('class', 'cell'), $receiver), $receiver, visitAndFinalize$lambda_0(createTable$lambda(name, defDate, table))), HTMLDivElement_0) ? tmp$ : throwCCE();
  }
  function makeDaysOffset(div, month) {
    var offset = calcOffset(month);
    addBlank(div, offset);
    return offset;
  }
  function calcOffset(month) {
    var date = new Date(month.year, month.month - 1 | 0, month.day);
    var offset = date.getDay() - 1 | 0;
    if (offset < 0)
      offset = 6;
    return offset;
  }
  function addBlank(div, count) {
    for (var i = 0; i < count; i++) {
      visitTag(new DIV_init(attributesMapOf('class', 'blank-day'), div.consumer), visit$lambda_3(div$lambda_1));
    }
  }
  function getMonth(date) {
    return date.getShortMonth() + ', ' + date.year;
  }
  function addOnclickDayEvent$lambda(closure$day, closure$tableName) {
    return function (it) {
      dayOnclickEvent(closure$day, closure$tableName);
      return Unit;
    };
  }
  function addOnclickDayEvent(tableName) {
    var tmp$, tmp$_0;
    var days = document.getElementsByClassName(tableName + '-day-button');
    tmp$ = days.length;
    for (var i = 0; i < tmp$; i++) {
      var day = Kotlin.isType(tmp$_0 = ensureNotNull(days[i]), HTMLInputElement) ? tmp$_0 : throwCCE();
      day.onclick = addOnclickDayEvent$lambda(day, tableName);
    }
  }
  function dayOnclickEvent$lambda$lambda(closure$date, closure$newState) {
    return function ($receiver) {
      var tmp$;
      setChosen(closure$date, closure$newState);
      if (closure$newState) {
        tmp$ = secondColor();
      } else {
        tmp$ = mainColor();
      }
      $receiver.background = tmp$;
      return Unit;
    };
  }
  function dayOnclickEvent$lambda(closure$day, closure$date, closure$newState) {
    return function (it) {
      var tmp$, tmp$_0;
      tmp$_0 = (Kotlin.isType(tmp$ = closure$day.parentElement, HTMLDivElement) ? tmp$ : throwCCE()).style;
      dayOnclickEvent$lambda$lambda(closure$date, closure$newState)(tmp$_0);
      return Unit;
    };
  }
  function dayOnclickEvent(day, tableName) {
    var date = day.id;
    var newState = !getChosen(date);
    var request = new XMLHttpRequest();
    request.open('POST', '/calendar');
    request.onload = dayOnclickEvent$lambda(day, date, newState);
    request.send(JSON.stringify(json([to('calendarName', tableName), to('date', date), to('state', newState)])));
  }
  function addOnclickArrowEvents$lambda(closure$topMonth, closure$tableName) {
    return function (it) {
      return it.deltaY > 0 ? moveDown(closure$topMonth, closure$tableName) : moveUp(closure$topMonth, closure$tableName);
    };
  }
  function addOnclickArrowEvents$lambda_0(closure$type, closure$topMonth, closure$tableName) {
    return function (it) {
      return equals(closure$type, 'up') ? moveUp(closure$topMonth, closure$tableName) : moveDown(closure$topMonth, closure$tableName);
    };
  }
  function addOnclickArrowEvents(tableName) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var arrows = document.getElementsByClassName(tableName + '-arrow');
    var topMonth = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById(tableName + '-01-2021-month')), HTMLDivElement) ? tmp$ : throwCCE();
    var monthFeed = Kotlin.isType(tmp$_0 = ensureNotNull(document.getElementById(tableName + '-month-feed')), HTMLDivElement) ? tmp$_0 : throwCCE();
    monthFeed.onwheel = addOnclickArrowEvents$lambda(topMonth, tableName);
    tmp$_1 = arrows.length;
    for (var i = 0; i < tmp$_1; i++) {
      var arrow = Kotlin.isType(tmp$_2 = ensureNotNull(arrows[i]), HTMLInputElement) ? tmp$_2 : throwCCE();
      var type = split(arrow.id, ['-']).get_za3lpa$(1);
      arrow.onclick = addOnclickArrowEvents$lambda_0(type, topMonth, tableName);
    }
  }
  var tablesSizes;
  var tableMoving;
  function moveUp$lambda(it) {
    return it >= 0;
  }
  function moveUp(topMonth, tableName) {
    return moveMonthFeed(topMonth, -1, tableName, moveUp$lambda);
  }
  function moveDown$lambda(closure$tableName) {
    return function (it) {
      return it < ensureNotNull(tablesSizes.get_11rb$(closure$tableName));
    };
  }
  function moveDown(topMonth, tableName) {
    return moveMonthFeed(topMonth, 1, tableName, moveDown$lambda(tableName));
  }
  function Coroutine$moveMonthFeed$lambda(closure$topMonth_0, closure$condition_0, closure$add_0, closure$tableName_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$topMonth = closure$topMonth_0;
    this.local$closure$condition = closure$condition_0;
    this.local$closure$add = closure$add_0;
    this.local$closure$tableName = closure$tableName_0;
  }
  Coroutine$moveMonthFeed$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$moveMonthFeed$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$moveMonthFeed$lambda.prototype.constructor = Coroutine$moveMonthFeed$lambda;
  Coroutine$moveMonthFeed$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var tableHeight = getTableHeight();
            var index = getCurrentMonthIndex(this.local$closure$topMonth, tableHeight);
            if (this.local$closure$condition(index + this.local$closure$add | 0) && !ensureNotNull(tableMoving.get_11rb$(this.local$closure$tableName))) {
              var $receiver = tableMoving;
              var key = this.local$closure$tableName;
              $receiver.put_xwzc9p$(key, true);
              this.state_0 = 2;
              this.result_0 = await_0(setMonth(this.local$closure$topMonth, index + this.local$closure$add | 0, tableHeight, this.local$closure$tableName), this);
              if (this.result_0 === COROUTINE_SUSPENDED)
                return COROUTINE_SUSPENDED;
              continue;
            } else {
              this.state_0 = 3;
              continue;
            }

          case 1:
            throw this.exception_0;
          case 2:
            var $receiver_0 = tableMoving;
            var key_0 = this.local$closure$tableName;
            $receiver_0.put_xwzc9p$(key_0, false);
            return Unit;
          case 3:
            return Unit;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function moveMonthFeed$lambda(closure$topMonth_0, closure$condition_0, closure$add_0, closure$tableName_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$moveMonthFeed$lambda(closure$topMonth_0, closure$condition_0, closure$add_0, closure$tableName_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function moveMonthFeed(topMonth, add, tableName, condition) {
    return promise(coroutines.GlobalScope, void 0, void 0, moveMonthFeed$lambda(topMonth, condition, add, tableName));
  }
  function getTableHeight() {
    var tmp$, tmp$_0;
    tmp$_0 = Kotlin.isType(tmp$ = ensureNotNull(document.getElementsByClassName('cell')[0]), HTMLDivElement) ? tmp$ : throwCCE();
    return toInt(dropLast(window.getComputedStyle(tmp$_0).height, 2));
  }
  function getCurrentMonthIndex(month, tableHeight) {
    var tmp$;
    try {
      var marginTop = toInt(dropLast(window.getComputedStyle(month).getPropertyValue('margin-top'), 2));
      tmp$ = marginTop / (-(tableHeight - 11 | 0) | 0) | 0;
    } catch (e) {
      if (Kotlin.isType(e, NumberFormatException)) {
        tmp$ = 0;
      } else
        throw e;
    }
    return tmp$;
  }
  function setMonth$lambda$lambda(closure$resolve) {
    return function (it) {
      closure$resolve(Unit);
      return Unit;
    };
  }
  function setMonth$lambda(closure$monthIndex, closure$cellDate, closure$tableHeight, closure$topMonth) {
    return function (resolve, f) {
      closure$cellDate.innerHTML = getMonth(new NormalDate(2021, closure$monthIndex, 1));
      closure$topMonth.style.marginTop = 'calc(-' + closure$monthIndex + ' * ' + closure$tableHeight + 'px + ' + (closure$monthIndex * 11 | 0) + 'px)';
      closure$topMonth.addEventListener('transitionend', setMonth$lambda$lambda(resolve));
      return Unit;
    };
  }
  function setMonth(topMonth, monthIndex, tableHeight, name) {
    var tmp$;
    var cellDate = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById(name + '-cell-date')), HTMLSpanElement) ? tmp$ : throwCCE();
    return new Promise(setMonth$lambda(monthIndex, cellDate, tableHeight, topMonth));
  }
  var monthSizes;
  var TABLES;
  function requestTables$lambda(closure$request) {
    return function (it) {
      var $receiver = closure$request;
      var serializer_0 = ListSerializer(serializer(kotlin_js_internal_StringCompanionObject));
      var $receiver_0 = Json.Default.decodeFromString_awif5v$(Response.Companion.serializer_swdriu$(serializer_0), $receiver.responseText).message;
      var destination = ArrayList_init(collectionSizeOrDefault($receiver_0, 10));
      var tmp$;
      tmp$ = $receiver_0.iterator();
      while (tmp$.hasNext()) {
        var item = tmp$.next();
        var tmp$_0 = destination.add_11rb$;
        setChosen(item, true);
        tmp$_0.call(destination, Unit);
      }
      addTables(TABLES);
      return Unit;
    };
  }
  function requestTables() {
    var request = new XMLHttpRequest();
    request.open('GET', '/calendar');
    request.onload = requestTables$lambda(request);
    request.send();
  }
  function setChosen(date, value) {
    var $receiver = split(date, ['/']);
    TABLES.get_za3lpa$(0).months.get_za3lpa$(toInt($receiver.get_za3lpa$(1)) - 1 | 0).days.get_za3lpa$(toInt($receiver.get_za3lpa$(0)) - 1 | 0).chosen = value;
  }
  function getChosen(date) {
    var $receiver = split(date, ['/']);
    return TABLES.get_za3lpa$(0).months.get_za3lpa$(toInt($receiver.get_za3lpa$(1)) - 1 | 0).days.get_za3lpa$(toInt($receiver.get_za3lpa$(0)) - 1 | 0).chosen;
  }
  function requestName$lambda(closure$request, closure$name) {
    return function (it) {
      var $receiver = closure$request;
      var serializer_0 = MapSerializer(serializer(kotlin_js_internal_StringCompanionObject), serializer(kotlin_js_internal_StringCompanionObject));
      var accountName = toString(Json.Default.decodeFromString_awif5v$(Response.Companion.serializer_swdriu$(serializer_0), $receiver.responseText).message.get_11rb$('name'));
      closure$name.innerHTML = accountName;
      return Unit;
    };
  }
  function requestName() {
    var tmp$;
    var name = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('acc-name')), HTMLSpanElement) ? tmp$ : throwCCE();
    var request = new XMLHttpRequest();
    request.open('POST', '/payload');
    request.onload = requestName$lambda(request, name);
    var $receiver = Json.Default;
    var value = ['name'];
    var tmp$_0;
    request.send($receiver.encodeToString_tf03ej$(Kotlin.isType(tmp$_0 = serializer_0($receiver.serializersModule, createKType(PrimitiveClasses$arrayClass, [createInvariantKTypeProjection(createKType(PrimitiveClasses$stringClass, [], false))], false)), KSerializer) ? tmp$_0 : throwCCE(), value));
  }
  function notFoundPage$lambda(it) {
    return Unit;
  }
  function notFoundPage() {
    window.onload = notFoundPage$lambda;
  }
  function registerPage$lambda(it) {
    sendAuthRequest('register', ['reg-input1', 'reg-input2', 'reg-input3', 'reg-input4']);
    addInputEvents();
    return Unit;
  }
  function registerPage() {
    window.onload = registerPage$lambda;
  }
  function visitAndFinalize$lambda_1(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visitAndFinalize$lambda_2(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_4(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_5(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_6(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visit$lambda_7(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  function visitAndFinalize$lambda_3(closure$block) {
    return function ($receiver) {
      closure$block($receiver);
      return Unit;
    };
  }
  var emailForm;
  var nameForm;
  var passwordForm;
  function deleteForm$lambda(it) {
    deleteEventGood();
    return Unit;
  }
  var deleteForm;
  function confirmDeleteForm$lambda(it) {
    window.location.replace('/login');
    return Unit;
  }
  function confirmDeleteForm$lambda_0(it) {
    confirmDeleteBadEvent(it);
    return Unit;
  }
  var confirmDeleteForm;
  function settingsPage$lambda(it) {
    setSettings([emailForm, nameForm, passwordForm, deleteForm]);
    setLogout();
    return Unit;
  }
  function settingsPage() {
    window.onload = settingsPage$lambda;
  }
  function setSettings(properties) {
    var tmp$;
    for (tmp$ = 0; tmp$ !== properties.length; ++tmp$) {
      var tmp$_0 = properties[tmp$];
      var type = tmp$_0.component1(), inputs = tmp$_0.component2(), eventGood = tmp$_0.component3(), eventBad = tmp$_0.component4();
      setRequestEvent('/auth', unionTypes(type) + '-btn', 'settings', inputs, eventGood, eventBad);
    }
  }
  function SettingsProperty(type, inputs, eventGood, eventBad) {
    if (eventGood === void 0)
      eventGood = SettingsProperty_init$lambda;
    if (eventBad === void 0)
      eventBad = SettingsProperty_init$lambda_0;
    this.type = type;
    this.inputs = inputs;
    this.eventGood = eventGood;
    this.eventBad = eventBad;
  }
  function SettingsProperty_init$lambda(it) {
    requestAccepted(it);
    return Unit;
  }
  function SettingsProperty_init$lambda_0(it) {
    requestRejected(it);
    return Unit;
  }
  SettingsProperty.$metadata$ = {kind: Kind_CLASS, simpleName: 'SettingsProperty', interfaces: []};
  SettingsProperty.prototype.component1 = function () {
    return this.type;
  };
  SettingsProperty.prototype.component2 = function () {
    return this.inputs;
  };
  SettingsProperty.prototype.component3 = function () {
    return this.eventGood;
  };
  SettingsProperty.prototype.component4 = function () {
    return this.eventBad;
  };
  SettingsProperty.prototype.copy_3gc26c$ = function (type, inputs, eventGood, eventBad) {
    return new SettingsProperty(type === void 0 ? this.type : type, inputs === void 0 ? this.inputs : inputs, eventGood === void 0 ? this.eventGood : eventGood, eventBad === void 0 ? this.eventBad : eventBad);
  };
  SettingsProperty.prototype.toString = function () {
    return 'SettingsProperty(type=' + Kotlin.toString(this.type) + (', inputs=' + Kotlin.toString(this.inputs)) + (', eventGood=' + Kotlin.toString(this.eventGood)) + (', eventBad=' + Kotlin.toString(this.eventBad)) + ')';
  };
  SettingsProperty.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.type) | 0;
    result = result * 31 + Kotlin.hashCode(this.inputs) | 0;
    result = result * 31 + Kotlin.hashCode(this.eventGood) | 0;
    result = result * 31 + Kotlin.hashCode(this.eventBad) | 0;
    return result;
  };
  SettingsProperty.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.type, other.type) && Kotlin.equals(this.inputs, other.inputs) && Kotlin.equals(this.eventGood, other.eventGood) && Kotlin.equals(this.eventBad, other.eventBad)))));
  };
  var promiseMap;
  function requestAccepted(type) {
    return requestCommon(type, secondColor(), goodNotificationText(type), 'Submitted');
  }
  function requestRejected(type) {
    return requestCommon(type, badColor(), badNotificationText(type), 'Rejected');
  }
  function requestCommon$lambda(it) {
    return it.innerHTML;
  }
  function requestCommon$lambda_0(el, value) {
    el.innerHTML = value;
    return Unit;
  }
  function requestCommon$lambda_1(closure$type, closure$color, closure$text, closure$buttonValue, closure$getField, closure$setField) {
    return function () {
      animateBlock(unionTypes(closure$type), closure$color, closure$text, closure$buttonValue, closure$getField, closure$setField);
      return Unit;
    };
  }
  function requestCommon(type, color, text, buttonValue, getField, setField) {
    if (getField === void 0)
      getField = requestCommon$lambda;
    if (setField === void 0)
      setField = requestCommon$lambda_0;
    return requestEvent(type, requestCommon$lambda_1(type, color, text, buttonValue, getField, setField));
  }
  function requestEvent$lambda$lambda(closure$event) {
    return function (resolve, f) {
      closure$event();
      resolve(Unit);
      return Unit;
    };
  }
  function Coroutine$requestEvent$lambda(closure$type_0, closure$event_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$type = closure$type_0;
    this.local$closure$event = closure$event_0;
  }
  Coroutine$requestEvent$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$requestEvent$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$requestEvent$lambda.prototype.constructor = Coroutine$requestEvent$lambda;
  Coroutine$requestEvent$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            if (!ensureNotNull(promiseMap.get_11rb$(this.local$closure$type))) {
              var $receiver = promiseMap;
              var key = this.local$closure$type;
              $receiver.put_xwzc9p$(key, true);
              this.state_0 = 2;
              this.result_0 = await_0(new Promise(requestEvent$lambda$lambda(this.local$closure$event)), this);
              if (this.result_0 === COROUTINE_SUSPENDED)
                return COROUTINE_SUSPENDED;
              continue;
            } else {
              this.state_0 = 3;
              continue;
            }

          case 1:
            throw this.exception_0;
          case 2:
            var $receiver_0 = promiseMap;
            var key_0 = this.local$closure$type;
            $receiver_0.put_xwzc9p$(key_0, false);
            return Unit;
          case 3:
            return Unit;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function requestEvent$lambda(closure$type_0, closure$event_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$requestEvent$lambda(closure$type_0, closure$event_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function requestEvent(type, event) {
    return promise(coroutines.GlobalScope, void 0, void 0, requestEvent$lambda(type, event));
  }
  function animateBlock(type, color, text, buttonValue, getField, setField) {
    colorHeader(type, color);
    showNotification(type, text, getField, setField);
    blockChangeButton(type, buttonValue);
  }
  function colorHeader$lambda(closure$color) {
    return function ($receiver) {
      $receiver.style.setProperty('animation-name', '');
      $receiver.getBoundingClientRect();
      $receiver.style.setProperty('--back-color', closure$color);
      $receiver.style.setProperty('animation-name', 'colorHeader');
      return Unit;
    };
  }
  function colorHeader(type, color) {
    var tmp$;
    colorHeader$lambda(color)(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('name-container-' + type)), HTMLDivElement) ? tmp$ : throwCCE());
  }
  function showNotification$lambda$lambda(closure$setField, closure$text) {
    return function (it) {
      closure$setField(it, closure$text);
      return Unit;
    };
  }
  function showNotification$lambda$lambda_0(closure$setField, closure$old) {
    return function (it) {
      closure$setField(it, closure$old);
      return Unit;
    };
  }
  function Coroutine$showNotification$lambda(closure$type_0, closure$getField_0, closure$setField_0, closure$text_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$type = closure$type_0;
    this.local$closure$getField = closure$getField_0;
    this.local$closure$setField = closure$setField_0;
    this.local$closure$text = closure$text_0;
    this.local$header = void 0;
    this.local$old = void 0;
  }
  Coroutine$showNotification$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$showNotification$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$showNotification$lambda.prototype.constructor = Coroutine$showNotification$lambda;
  Coroutine$showNotification$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var tmp$;
            this.local$header = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('header-text-' + this.local$closure$type)), HTMLElement) ? tmp$ : throwCCE();
            this.local$old = this.local$closure$getField(this.local$header);
            this.state_0 = 2;
            this.result_0 = await_0(changeHeader(this.local$header, showNotification$lambda$lambda(this.local$closure$setField, this.local$closure$text)), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            this.state_0 = 3;
            this.result_0 = delay(L1000, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 3:
            this.state_0 = 4;
            this.result_0 = await_0(changeHeader(this.local$header, showNotification$lambda$lambda_0(this.local$closure$setField, this.local$old)), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 4:
            return this.result_0;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function showNotification$lambda(closure$type_0, closure$getField_0, closure$setField_0, closure$text_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$showNotification$lambda(closure$type_0, closure$getField_0, closure$setField_0, closure$text_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function showNotification(type, text, getField, setField) {
    return promise(coroutines.GlobalScope, void 0, void 0, showNotification$lambda(type, getField, setField, text));
  }
  function changeHeader$lambda(it) {
    close(it);
    return Unit;
  }
  function changeHeader$lambda$lambda(it) {
    open(it);
    return Unit;
  }
  function changeHeader$lambda_0(closure$changeText, closure$header) {
    return function (it) {
      closure$changeText(closure$header);
      return animate(closure$header, changeHeader$lambda$lambda);
    };
  }
  function changeHeader(header, changeText) {
    return animate(header, changeHeader$lambda).then(changeHeader$lambda_0(changeText, header));
  }
  function animate$lambda$lambda(closure$resolve) {
    return function (it) {
      closure$resolve(Unit);
      return Unit;
    };
  }
  function animate$lambda(closure$animation, closure$element) {
    return function (resolve, f) {
      closure$animation(closure$element);
      var tmp$ = closure$element;
      var o = {};
      o['passive'] = false;
      o['once'] = false;
      o['capture'] = false;
      tmp$.addEventListener('transitionend', animate$lambda$lambda(resolve), to(o.once, true));
      return Unit;
    };
  }
  function animate(element, animation) {
    return new Promise(animate$lambda(animation, element));
  }
  function close($receiver) {
    $receiver.style.width = $receiver.scrollWidth.toString() + 'px';
    $receiver.getBoundingClientRect();
    $receiver.style.width = '0';
  }
  function open($receiver) {
    $receiver.style.width = $receiver.scrollWidth.toString() + 'px';
  }
  function blockChangeButton$lambda(closure$blockText) {
    return function (btn) {
      return buttonShowMessage(btn, closure$blockText);
    };
  }
  function blockChangeButton(type, blockText) {
    var tmp$;
    blockButton(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById(type + '-btn')), HTMLInputElement) ? tmp$ : throwCCE(), blockChangeButton$lambda(blockText));
  }
  function blockButton$lambda$lambda(it) {
    return Unit;
  }
  function Coroutine$blockButton$lambda(closure$button_0, closure$action_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$button = closure$button_0;
    this.local$closure$action = closure$action_0;
    this.local$onclick = void 0;
  }
  Coroutine$blockButton$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$blockButton$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$blockButton$lambda.prototype.constructor = Coroutine$blockButton$lambda;
  Coroutine$blockButton$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.local$onclick = this.local$closure$button.onclick;
            this.local$closure$button.onclick = blockButton$lambda$lambda;
            this.local$closure$button.style.cursor = 'default';
            this.state_0 = 2;
            this.result_0 = await_0(this.local$closure$action(this.local$closure$button), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            this.local$closure$button.style.cursor = 'pointer';
            return this.local$closure$button.onclick = this.local$onclick, Unit;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function blockButton$lambda(closure$button_0, closure$action_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$blockButton$lambda(closure$button_0, closure$action_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function blockButton(button, action) {
    return promise(coroutines.GlobalScope, void 0, void 0, blockButton$lambda(button, action));
  }
  function Coroutine$buttonShowMessage$lambda(closure$button_0, closure$msg_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$button = closure$button_0;
    this.local$closure$msg = closure$msg_0;
    this.local$old = void 0;
  }
  Coroutine$buttonShowMessage$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$buttonShowMessage$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$buttonShowMessage$lambda.prototype.constructor = Coroutine$buttonShowMessage$lambda;
  Coroutine$buttonShowMessage$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.local$old = this.local$closure$button.value;
            this.state_0 = 2;
            this.result_0 = await_0(buttonChange(this.local$closure$button, this.local$closure$msg), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            this.state_0 = 3;
            this.result_0 = delay(L1000, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 3:
            this.state_0 = 4;
            this.result_0 = await_0(buttonChange(this.local$closure$button, this.local$old), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 4:
            return Unit;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function buttonShowMessage$lambda(closure$button_0, closure$msg_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$buttonShowMessage$lambda(closure$button_0, closure$msg_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function buttonShowMessage(button, msg) {
    return promise(coroutines.GlobalScope, void 0, void 0, buttonShowMessage$lambda(button, msg));
  }
  function buttonChange$lambda(it) {
    var tmp$;
    fadeOut(Kotlin.isType(tmp$ = it, HTMLInputElement) ? tmp$ : throwCCE());
    return Unit;
  }
  function buttonChange$lambda$lambda(it) {
    var tmp$;
    fadeIn(Kotlin.isType(tmp$ = it, HTMLInputElement) ? tmp$ : throwCCE());
    return Unit;
  }
  function buttonChange$lambda_0(closure$text, closure$button) {
    return function (it) {
      closure$button.value = closure$text;
      return animate(closure$button, buttonChange$lambda$lambda);
    };
  }
  function buttonChange(button, text) {
    return animate(button, buttonChange$lambda).then(buttonChange$lambda_0(text, button));
  }
  function fadeOut($receiver) {
    $receiver.style.opacity = '0';
  }
  function fadeIn($receiver) {
    $receiver.style.opacity = '1';
  }
  function goodNotificationText(serverMessage) {
    var tmp$;
    switch (serverMessage) {
      case 'email':
        tmp$ = 'Your email was changed';
        break;
      case 'name':
        tmp$ = 'Your account name was changed';
        break;
      case 'password':
        tmp$ = 'Your password was changed';
        break;
      default:tmp$ = 'Successful!';
        break;
    }
    return tmp$;
  }
  function badNotificationText(serverMessage) {
    var tmp$;
    switch (serverMessage) {
      case 'email':
        tmp$ = 'Wrong email format';
        break;
      case 'name':
        tmp$ = 'This name is already taken';
        break;
      case 'password':
        tmp$ = 'Password does not meet requirements';
        break;
      case 'repeat-password':
        tmp$ = 'Passwords should match';
        break;
      case 'delete':
        tmp$ = 'Entered name is wrong';
        break;
      case 'confirm-delete':
        tmp$ = 'Wrong Password';
        break;
      default:tmp$ = 'Request failed, try again later';
        break;
    }
    return tmp$;
  }
  function unionTypes(type) {
    return last(split(type, ['-']));
  }
  function deleteEventGood$lambda() {
    return getConfirmDeleteReturnButton();
  }
  function deleteEventGood$lambda_0(it) {
    var tmp$;
    (Kotlin.isType(tmp$ = it, HTMLInputElement) ? tmp$ : throwCCE()).value = 'Return back';
    return Unit;
  }
  function deleteEventGood$lambda$lambda(it) {
    returnToDeletePage();
    return Unit;
  }
  function deleteEventGood$lambda_1() {
    var tmp$;
    var headerButton = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('header-text-delete')), HTMLInputElement) ? tmp$ : throwCCE();
    headerButton.disabled = false;
    headerButton.style.cursor = 'pointer';
    headerButton.onclick = deleteEventGood$lambda$lambda;
    return Unit;
  }
  function deleteEventGood() {
    changePage('confirm-delete', 'delete', '100%', '-100%', deleteEventGood$lambda, deleteEventGood$lambda_0, 'CONFIRM', confirmDeleteForm, deleteEventGood$lambda_1);
  }
  function returnToDeletePage$lambda() {
    return getOldDeleteHeader();
  }
  function returnToDeletePage$lambda_0(it) {
    var tmp$;
    (Kotlin.isType(tmp$ = it, HTMLSpanElement) ? tmp$ : throwCCE()).innerHTML = 'Delete Account';
    return Unit;
  }
  function returnToDeletePage() {
    changePage('delete', 'confirm-delete', '-100%', '100%', returnToDeletePage$lambda, returnToDeletePage$lambda_0, 'Delete', deleteForm);
  }
  function changePage$lambda() {
    return Unit;
  }
  function changePage$lambda$lambda(closure$getNewHeaderElement) {
    return function () {
      return closure$getNewHeaderElement();
    };
  }
  function changePage$lambda_0(closure$getNewHeaderElement) {
    return function (resolve, f) {
      changeDeleteHeader(changePage$lambda$lambda(closure$getNewHeaderElement));
      resolve(Unit);
      return Unit;
    };
  }
  function changePage$lambda$lambda$lambda(it) {
    println(it);
    return Unit;
  }
  function changePage$lambda$lambda$lambda_0(closure$setNewHeaderText) {
    return function (header) {
      closure$setNewHeaderText(header);
      return Unit;
    };
  }
  function changePage$lambda$lambda_0(closure$id, closure$oldPageId, closure$newPageStartLeft, closure$oldPageEndLeft, closure$setNewHeaderText, closure$newButtonText) {
    return function (button) {
      var tmp$;
      slidePage(closure$id, closure$oldPageId, closure$newPageStartLeft, closure$oldPageEndLeft).catch(changePage$lambda$lambda$lambda);
      changeHeader(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('header-text-delete')), HTMLElement) ? tmp$ : throwCCE(), changePage$lambda$lambda$lambda_0(closure$setNewHeaderText));
      return buttonChange(button, closure$newButtonText);
    };
  }
  function changePage$lambda_1(closure$id, closure$oldPageId, closure$newPageStartLeft, closure$oldPageEndLeft, closure$setNewHeaderText, closure$newButtonText) {
    return function (it) {
      var tmp$;
      return blockButton(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('delete-btn')), HTMLInputElement) ? tmp$ : throwCCE(), changePage$lambda$lambda_0(closure$id, closure$oldPageId, closure$newPageStartLeft, closure$oldPageEndLeft, closure$setNewHeaderText, closure$newButtonText));
    };
  }
  function changePage$lambda_2(closure$from, closure$onEnd) {
    return function (it) {
      addPageEvent(closure$from);
      closure$onEnd();
      return Unit;
    };
  }
  function changePage(id, oldPageId, newPageStartLeft, oldPageEndLeft, getNewHeaderElement, setNewHeaderText, newButtonText, from, onEnd) {
    if (onEnd === void 0)
      onEnd = changePage$lambda;
    (new Promise(changePage$lambda_0(getNewHeaderElement))).then(changePage$lambda_1(id, oldPageId, newPageStartLeft, oldPageEndLeft, setNewHeaderText, newButtonText)).then(changePage$lambda_2(from, onEnd));
  }
  function changeDeleteHeader(newHeader) {
    var tmp$;
    var oldHeader = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('header-text-delete')), HTMLElement) ? tmp$ : throwCCE();
    var parent = ensureNotNull(oldHeader.parentNode);
    parent.removeChild(oldHeader);
    parent.appendChild(newHeader());
  }
  function getConfirmDeleteReturnButton$lambda($receiver) {
    set_id($receiver, 'header-text-delete');
    $receiver.value = 'Delete Account';
    $receiver.disabled = true;
    set_style($receiver, 'cursor: default;');
    return Unit;
  }
  function getConfirmDeleteReturnButton() {
    var $receiver = get_create(document);
    var type = InputType.button;
    return visitTagAndFinalize(new INPUT_init(attributesMapOf_0(['type', type != null ? enumEncode(type) : null, 'formenctype', null != null ? enumEncode(null) : null, 'formmethod', null != null ? enumEncode(null) : null, 'name', null, 'class', 'header-text']), $receiver), $receiver, visitAndFinalize$lambda_1(getConfirmDeleteReturnButton$lambda));
  }
  function getOldDeleteHeader$lambda($receiver) {
    set_id($receiver, 'header-text-delete');
    $receiver.unaryPlus_pdl1vz$('Return back');
    return Unit;
  }
  function getOldDeleteHeader() {
    var $receiver = get_create(document);
    return visitTagAndFinalize(new SPAN_init(attributesMapOf('class', 'header-text'), $receiver), $receiver, visitAndFinalize$lambda_2(getOldDeleteHeader$lambda));
  }
  function slidePage$lambda(closure$oldPageId, closure$id, closure$newPageStartLeft, closure$oldPageEndLeft) {
    return function (resolve, f) {
      var tmp$, tmp$_0;
      var oldPage = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('page-' + closure$oldPageId)), HTMLDivElement) ? tmp$ : throwCCE();
      ensureNotNull(oldPage.parentNode).appendChild(getNewPage(closure$id));
      var newPage = Kotlin.isType(tmp$_0 = ensureNotNull(document.getElementById('page-' + closure$id)), HTMLDivElement) ? tmp$_0 : throwCCE();
      newPage.style.left = closure$newPageStartLeft;
      newPage.getBoundingClientRect();
      changePage_0(oldPage, newPage, closure$oldPageEndLeft);
      resolve(Unit);
      return Unit;
    };
  }
  function slidePage(id, oldPageId, newPageStartLeft, oldPageEndLeft) {
    return new Promise(slidePage$lambda(oldPageId, id, newPageStartLeft, oldPageEndLeft));
  }
  function Coroutine$changePage$lambda(closure$oldPage_0, closure$oldPageLeft_0, closure$newPage_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$oldPage = closure$oldPage_0;
    this.local$closure$oldPageLeft = closure$oldPageLeft_0;
    this.local$closure$newPage = closure$newPage_0;
  }
  Coroutine$changePage$lambda.$metadata$ = {kind: Kotlin.Kind.CLASS, simpleName: null, interfaces: [CoroutineImpl]};
  Coroutine$changePage$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$changePage$lambda.prototype.constructor = Coroutine$changePage$lambda;
  Coroutine$changePage$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            movePage(this.local$closure$oldPage, this.local$closure$oldPageLeft);
            movePage(this.local$closure$newPage, '0');
            this.state_0 = 2;
            this.result_0 = delay(L1000, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            return ensureNotNull(this.local$closure$oldPage.parentNode).removeChild(this.local$closure$oldPage);
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function changePage$lambda_3(closure$oldPage_0, closure$oldPageLeft_0, closure$newPage_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$changePage$lambda(closure$oldPage_0, closure$oldPageLeft_0, closure$newPage_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function changePage_0(oldPage, newPage, oldPageLeft) {
    return promise(coroutines.GlobalScope, void 0, void 0, changePage$lambda_3(oldPage, oldPageLeft, newPage));
  }
  function movePage(page, left) {
    page.style.left = left;
  }
  function getNewPage(id) {
    var tmp$;
    switch (id) {
      case 'delete':
        tmp$ = getDeletePage();
        break;
      case 'confirm-delete':
        tmp$ = getConfirmDeletePage();
        break;
      default:throw IllegalArgumentException_init('Wrong id');
    }
    return tmp$;
  }
  function getConfirmDeletePage() {
    return getInputForm('confirm-delete', 'Enter your password to confirm deletion', InputType.text, 'Password', 'After pressing CONFIRM button your account will be immediately deleted');
  }
  function getDeletePage() {
    return getInputForm('delete', 'Enter your account name to confirm deletion', InputType.text, 'Account Name', 'WARNING! This action cannot be undone! Your account will be deleted completely!');
  }
  function getInputForm$lambda$lambda$lambda$lambda(closure$label) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$label);
      return Unit;
    };
  }
  function getInputForm$lambda$lambda$lambda$lambda_0(closure$inputType, closure$id, closure$inputPlaceholder) {
    return function ($receiver) {
      $receiver.type = closure$inputType;
      set_id($receiver, 'input-' + closure$id);
      $receiver.placeholder = closure$inputPlaceholder;
      return Unit;
    };
  }
  function getInputForm$lambda$lambda$lambda$lambda_1(closure$caption) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$caption);
      return Unit;
    };
  }
  function getInputForm$lambda$lambda$lambda(closure$id, closure$label, closure$inputType, closure$inputPlaceholder, closure$caption) {
    return function ($receiver) {
      set_id($receiver, 'f-' + closure$id);
      var block = getInputForm$lambda$lambda$lambda$lambda(closure$label);
      visitTag(new LABEL_init(attributesMapOf('class', null), $receiver.consumer), visit$lambda_4(block));
      var block_0 = getInputForm$lambda$lambda$lambda$lambda_0(closure$inputType, closure$id, closure$inputPlaceholder);
      visitTag(new INPUT_init(attributesMapOf_0(['type', null != null ? enumEncode(null) : null, 'formenctype', null != null ? enumEncode(null) : null, 'formmethod', null != null ? enumEncode(null) : null, 'name', null, 'class', null]), $receiver.consumer), visit$lambda_5(block_0));
      var block_1 = getInputForm$lambda$lambda$lambda$lambda_1(closure$caption);
      visitTag(new SPAN_init(attributesMapOf('class', 'caption'), $receiver.consumer), visit$lambda_6(block_1));
      return Unit;
    };
  }
  function getInputForm$lambda$lambda(closure$id, closure$label, closure$inputType, closure$inputPlaceholder, closure$caption) {
    return function ($receiver) {
      var block = getInputForm$lambda$lambda$lambda(closure$id, closure$label, closure$inputType, closure$inputPlaceholder, closure$caption);
      visitTag(new DIV_init(attributesMapOf('class', 'field'), $receiver.consumer), visit$lambda_7(block));
      return Unit;
    };
  }
  function getInputForm$lambda(closure$id, closure$label, closure$inputType, closure$inputPlaceholder, closure$caption) {
    return function ($receiver) {
      set_id($receiver, 'page-' + closure$id);
      var classes = 'field-container';
      var block = getInputForm$lambda$lambda(closure$id, closure$label, closure$inputType, closure$inputPlaceholder, closure$caption);
      visitTag(new DIV_init(attributesMapOf('class', classes), $receiver.consumer), visit$lambda_7(block));
      return Unit;
    };
  }
  function getInputForm(id, label, inputType, inputPlaceholder, caption) {
    var $receiver = get_create(document);
    var classes = 'change-page';
    var tmp$;
    return Kotlin.isType(tmp$ = visitTagAndFinalize(new DIV_init(attributesMapOf('class', classes), $receiver), $receiver, visitAndFinalize$lambda_3(getInputForm$lambda(id, label, inputType, inputPlaceholder, caption))), HTMLDivElement_0) ? tmp$ : throwCCE();
  }
  function addPageEvent(form) {
    setSettings([form]);
  }
  function confirmDeleteBadEvent$lambda$lambda(it) {
    var tmp$;
    return (Kotlin.isType(tmp$ = it, HTMLInputElement) ? tmp$ : throwCCE()).value;
  }
  function confirmDeleteBadEvent$lambda$lambda_0(el, value) {
    var tmp$;
    (Kotlin.isType(tmp$ = el, HTMLInputElement) ? tmp$ : throwCCE()).value = value;
    return Unit;
  }
  function confirmDeleteBadEvent$lambda(closure$response) {
    return function (it) {
      return requestCommon(closure$response, badColor(), badNotificationText(closure$response), 'Rejected', confirmDeleteBadEvent$lambda$lambda, confirmDeleteBadEvent$lambda$lambda_0);
    };
  }
  function confirmDeleteBadEvent(response) {
    var tmp$;
    blockButton(Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('header-text-delete')), HTMLInputElement) ? tmp$ : throwCCE(), confirmDeleteBadEvent$lambda(response));
  }
  function setLogout$lambda$lambda(it) {
    window.location.replace('/login');
    return Unit;
  }
  function setLogout$lambda(it) {
    var request = new XMLHttpRequest();
    request.open('POST', '/logout');
    request.onload = setLogout$lambda$lambda;
    request.send();
    return Unit;
  }
  function setLogout() {
    var tmp$;
    var button = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById('logout')), HTMLInputElement) ? tmp$ : throwCCE();
    button.addEventListener('click', setLogout$lambda);
  }
  var root;
  function mainColor() {
    return window.getComputedStyle(root).getPropertyValue('--main-color');
  }
  function secondColor() {
    return window.getComputedStyle(root).getPropertyValue('--second-color');
  }
  function badColor() {
    return window.getComputedStyle(root).getPropertyValue('--bad-color');
  }
  function setRequestEvent$lambda$lambda(closure$request, closure$eventGood, closure$eventBad) {
    return function (it) {
      var $receiver = closure$request;
      var serializer_0 = serializer(kotlin_js_internal_StringCompanionObject);
      var response = Json.Default.decodeFromString_awif5v$(Response.Companion.serializer_swdriu$(serializer_0), $receiver.responseText);
      var status = response.status;
      var message = response.message;
      if (equals(status, 'OK')) {
        closure$eventGood(message);
      } else {
        closure$eventBad(message);
      }
      return Unit;
    };
  }
  function setRequestEvent$lambda(closure$url, closure$eventGood, closure$eventBad, closure$inputs, closure$type) {
    return function (it) {
      var request = new XMLHttpRequest();
      request.open('POST', closure$url);
      request.onload = setRequestEvent$lambda$lambda(request, closure$eventGood, closure$eventBad);
      request.send(jsonifyForm(closure$inputs, closure$type));
      return Unit;
    };
  }
  function setRequestEvent(url, btn, type, inputs, eventGood, eventBad) {
    var tmp$;
    var btnElement = Kotlin.isType(tmp$ = ensureNotNull(document.getElementById(btn)), HTMLInputElement) ? tmp$ : throwCCE();
    btnElement.onclick = setRequestEvent$lambda(url, eventGood, eventBad, inputs, type);
  }
  function jsonifyForm(inputs, type) {
    var tmp$, tmp$_0, tmp$_1;
    var retval = LinkedHashMap_init();
    for (tmp$ = 0; tmp$ !== inputs.length; ++tmp$) {
      var i = inputs[tmp$];
      var it = Kotlin.isType(tmp$_0 = ensureNotNull(document.getElementById(i)), HTMLInputElement) ? tmp$_0 : throwCCE();
      retval.put_xwzc9p$(it.id, it.value);
    }
    if ((tmp$_1 = document.getElementById('__AntiCSRFToken')) != null) {
      var tmp$_2;
      var $receiver = Kotlin.isType(tmp$_2 = tmp$_1, HTMLInputElement) ? tmp$_2 : throwCCE();
      retval.put_xwzc9p$($receiver.id, $receiver.value);
    }var $receiver_0 = Json.Default;
    var value = new RequestInputs(type, retval);
    var tmp$_3;
    return $receiver_0.encodeToString_tf03ej$(Kotlin.isType(tmp$_3 = serializer_0($receiver_0.serializersModule, createKType(getKClass(RequestInputs), [], false)), KSerializer) ? tmp$_3 : throwCCE(), value);
  }
  function RequestInputs(type, inputs) {
    RequestInputs$Companion_getInstance();
    this.type = type;
    this.inputs = inputs;
  }
  function RequestInputs$Companion() {
    RequestInputs$Companion_instance = this;
  }
  RequestInputs$Companion.prototype.serializer = function () {
    return RequestInputs$$serializer_getInstance();
  };
  RequestInputs$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: []};
  var RequestInputs$Companion_instance = null;
  function RequestInputs$Companion_getInstance() {
    if (RequestInputs$Companion_instance === null) {
      new RequestInputs$Companion();
    }return RequestInputs$Companion_instance;
  }
  function RequestInputs$$serializer() {
    this.descriptor_kcjmwt$_0 = new PluginGeneratedSerialDescriptor('RequestInputs', this, 2);
    this.descriptor.addElement_ivxn3r$('type', false);
    this.descriptor.addElement_ivxn3r$('inputs', false);
    RequestInputs$$serializer_instance = this;
  }
  Object.defineProperty(RequestInputs$$serializer.prototype, 'descriptor', {configurable: true, get: function () {
    return this.descriptor_kcjmwt$_0;
  }});
  RequestInputs$$serializer.prototype.serialize_55azsf$ = function (encoder, value) {
    var output = encoder.beginStructure_24f42q$(this.descriptor);
    output.encodeStringElement_iij8qq$(this.descriptor, 0, value.type);
    output.encodeSerializableElement_r4qlx7$(this.descriptor, 1, new LinkedHashMapSerializer(internal.StringSerializer, internal.StringSerializer), value.inputs);
    output.endStructure_24f42q$(this.descriptor);
  };
  RequestInputs$$serializer.prototype.deserialize_bq71mq$ = function (decoder) {
    var index;
    var bitMask0 = 0;
    var local0, local1;
    var input = decoder.beginStructure_24f42q$(this.descriptor);
    loopLabel: while (true) {
      index = input.decodeElementIndex_24f42q$(this.descriptor);
      switch (index) {
        case 0:
          local0 = input.decodeStringElement_szpzho$(this.descriptor, 0);
          bitMask0 |= 1;
          break;
        case 1:
          local1 = input.decodeSerializableElement_12e8id$(this.descriptor, 1, new LinkedHashMapSerializer(internal.StringSerializer, internal.StringSerializer), local1);
          bitMask0 |= 2;
          break;
        case -1:
          break loopLabel;
        default:throw new UnknownFieldException(index);
      }
    }
    input.endStructure_24f42q$(this.descriptor);
    return RequestInputs_init(bitMask0, local0, local1, null);
  };
  RequestInputs$$serializer.prototype.childSerializers = function () {
    return [internal.StringSerializer, new LinkedHashMapSerializer(internal.StringSerializer, internal.StringSerializer)];
  };
  RequestInputs$$serializer.$metadata$ = {kind: Kind_OBJECT, simpleName: '$serializer', interfaces: [GeneratedSerializer]};
  var RequestInputs$$serializer_instance = null;
  function RequestInputs$$serializer_getInstance() {
    if (RequestInputs$$serializer_instance === null) {
      new RequestInputs$$serializer();
    }return RequestInputs$$serializer_instance;
  }
  function RequestInputs_init(seen1, type, inputs, serializationConstructorMarker) {
    var $this = serializationConstructorMarker || Object.create(RequestInputs.prototype);
    if ((seen1 & 1) === 0)
      throw new MissingFieldException('type');
    else
      $this.type = type;
    if ((seen1 & 2) === 0)
      throw new MissingFieldException('inputs');
    else
      $this.inputs = inputs;
    return $this;
  }
  RequestInputs.$metadata$ = {kind: Kind_CLASS, simpleName: 'RequestInputs', interfaces: []};
  RequestInputs.prototype.component1 = function () {
    return this.type;
  };
  RequestInputs.prototype.component2 = function () {
    return this.inputs;
  };
  RequestInputs.prototype.copy_mvjluj$ = function (type, inputs) {
    return new RequestInputs(type === void 0 ? this.type : type, inputs === void 0 ? this.inputs : inputs);
  };
  RequestInputs.prototype.toString = function () {
    return 'RequestInputs(type=' + Kotlin.toString(this.type) + (', inputs=' + Kotlin.toString(this.inputs)) + ')';
  };
  RequestInputs.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.type) | 0;
    result = result * 31 + Kotlin.hashCode(this.inputs) | 0;
    return result;
  };
  RequestInputs.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.type, other.type) && Kotlin.equals(this.inputs, other.inputs)))));
  };
  function Response(status, message) {
    Response$Companion_getInstance();
    this.status = status;
    this.message = message;
  }
  function Response$Companion() {
    Response$Companion_instance = this;
  }
  Response$Companion.prototype.serializer_swdriu$ = function (typeSerial0) {
    return Response$Response$$serializer_init(typeSerial0);
  };
  Response$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: [SerializerFactory]};
  var Response$Companion_instance = null;
  function Response$Companion_getInstance() {
    if (Response$Companion_instance === null) {
      new Response$Companion();
    }return Response$Companion_instance;
  }
  function Response$$serializer() {
    this.descriptor_3uuzty$_0 = new PluginGeneratedSerialDescriptor('Response', this, 2);
    this.descriptor.addElement_ivxn3r$('status', false);
    this.descriptor.addElement_ivxn3r$('message', false);
  }
  Object.defineProperty(Response$$serializer.prototype, 'descriptor', {configurable: true, get: function () {
    return this.descriptor_3uuzty$_0;
  }});
  Response$$serializer.prototype.serialize_55azsf$ = function (encoder, value) {
    var output = encoder.beginStructure_24f42q$(this.descriptor);
    output.encodeStringElement_iij8qq$(this.descriptor, 0, value.status);
    output.encodeSerializableElement_r4qlx7$(this.descriptor, 1, this.typeSerial0, value.message);
    output.endStructure_24f42q$(this.descriptor);
  };
  Response$$serializer.prototype.deserialize_bq71mq$ = function (decoder) {
    var index;
    var bitMask0 = 0;
    var local0, local1;
    var input = decoder.beginStructure_24f42q$(this.descriptor);
    loopLabel: while (true) {
      index = input.decodeElementIndex_24f42q$(this.descriptor);
      switch (index) {
        case 0:
          local0 = input.decodeStringElement_szpzho$(this.descriptor, 0);
          bitMask0 |= 1;
          break;
        case 1:
          local1 = input.decodeSerializableElement_12e8id$(this.descriptor, 1, this.typeSerial0, local1);
          bitMask0 |= 2;
          break;
        case -1:
          break loopLabel;
        default:throw new UnknownFieldException(index);
      }
    }
    input.endStructure_24f42q$(this.descriptor);
    return Response_init(bitMask0, local0, local1, null);
  };
  Response$$serializer.prototype.childSerializers = function () {
    return [internal.StringSerializer, this.typeSerial0];
  };
  Response$$serializer.prototype.typeParametersSerializers = function () {
    return [this.typeSerial0];
  };
  function Response$Response$$serializer_init(typeSerial0) {
    var $this = new Response$$serializer();
    $this.typeSerial0 = typeSerial0;
    return $this;
  }
  Response$$serializer.$metadata$ = {kind: Kind_CLASS, simpleName: '$serializer', interfaces: [GeneratedSerializer]};
  function Response_init(seen1, status, message, serializationConstructorMarker) {
    var $this = serializationConstructorMarker || Object.create(Response.prototype);
    if ((seen1 & 1) === 0)
      throw new MissingFieldException('status');
    else
      $this.status = status;
    if ((seen1 & 2) === 0)
      throw new MissingFieldException('message');
    else
      $this.message = message;
    return $this;
  }
  Response.$metadata$ = {kind: Kind_CLASS, simpleName: 'Response', interfaces: []};
  Response.prototype.component1 = function () {
    return this.status;
  };
  Response.prototype.component2 = function () {
    return this.message;
  };
  Response.prototype.copy_yuqcw7$ = function (status, message) {
    return new Response(status === void 0 ? this.status : status, message === void 0 ? this.message : message);
  };
  Response.prototype.toString = function () {
    return 'Response(status=' + Kotlin.toString(this.status) + (', message=' + Kotlin.toString(this.message)) + ')';
  };
  Response.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.status) | 0;
    result = result * 31 + Kotlin.hashCode(this.message) | 0;
    return result;
  };
  Response.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.status, other.status) && Kotlin.equals(this.message, other.message)))));
  };
  _.main = main;
  _.onloadPageByName_61zpoe$ = onloadPageByName;
  _.getPageJSFileName_61zpoe$ = getPageJSFileName;
  _.loginPage = loginPage;
  _.sendAuthRequest_c1kmwu$ = sendAuthRequest;
  _.addErrorMessage_puj7f4$ = addErrorMessage;
  $$importsForInline$$['kotlinx-html-js'] = $module$kotlinx_html_js;
  _.createErrorBlock_puj7f4$ = createErrorBlock;
  _.addInputEvents = addInputEvents;
  _.mainPage = mainPage;
  _.headerHeight = headerHeight;
  _.setHideShowButton = setHideShowButton;
  _.expandMainBlock_6taknv$ = expandMainBlock;
  _.hideInfoBar_6taknv$ = hideInfoBar;
  _.changeButtonText_isk9as$ = changeButtonText;
  _.Table = Table;
  _.Month = Month;
  _.Day = Day;
  _.NormalDate = NormalDate;
  _.addTables_a6tvh$ = addTables;
  _.addTable_mv64qd$ = addTable;
  _.createTable_1byqwe$ = createTable;
  _.makeDaysOffset_ptjhoc$ = makeDaysOffset;
  _.calcOffset_c6yqmt$ = calcOffset;
  _.addBlank_lniunr$ = addBlank;
  _.getMonth_c6yqmt$ = getMonth;
  _.addOnclickDayEvent_61zpoe$ = addOnclickDayEvent;
  _.dayOnclickEvent_isk9as$ = dayOnclickEvent;
  _.addOnclickArrowEvents_61zpoe$ = addOnclickArrowEvents;
  _.moveUp_h2l30b$ = moveUp;
  _.moveDown_h2l30b$ = moveDown;
  _.moveMonthFeed_ikg62i$ = moveMonthFeed;
  _.getTableHeight = getTableHeight;
  _.getCurrentMonthIndex_yx43qz$ = getCurrentMonthIndex;
  _.setMonth_3vq4qj$ = setMonth;
  _.requestTables = requestTables;
  _.setChosen_ivxn3r$ = setChosen;
  _.getChosen_61zpoe$ = getChosen;
  $$importsForInline$$['kotlinx-serialization-kotlinx-serialization-core-jsLegacy'] = $module$kotlinx_serialization_kotlinx_serialization_core_jsLegacy;
  _.requestName = requestName;
  _.notFoundPage = notFoundPage;
  _.registerPage = registerPage;
  _.settingsPage = settingsPage;
  _.setSettings_1fy1jq$ = setSettings;
  _.SettingsProperty = SettingsProperty;
  _.requestAccepted_61zpoe$ = requestAccepted;
  _.requestRejected_61zpoe$ = requestRejected;
  _.requestCommon_d1utzz$ = requestCommon;
  _.requestEvent_a4mwiz$ = requestEvent;
  _.animateBlock_d1utzz$ = animateBlock;
  _.colorHeader_puj7f4$ = colorHeader;
  _.showNotification_xhp5mt$ = showNotification;
  _.changeHeader_he2pit$ = changeHeader;
  _.animate_he2pit$ = animate;
  _.close_y4uc6z$ = close;
  _.open_y4uc6z$ = open;
  _.blockChangeButton_puj7f4$ = blockChangeButton;
  _.blockButton_m0slyv$ = blockButton;
  _.buttonShowMessage_isk9as$ = buttonShowMessage;
  _.buttonChange_isk9as$ = buttonChange;
  _.fadeOut_rsvr4l$ = fadeOut;
  _.fadeIn_rsvr4l$ = fadeIn;
  _.goodNotificationText_61zpoe$ = goodNotificationText;
  _.badNotificationText_61zpoe$ = badNotificationText;
  _.unionTypes_61zpoe$ = unionTypes;
  _.deleteEventGood = deleteEventGood;
  _.returnToDeletePage = returnToDeletePage;
  _.changePage_7qlx9o$ = changePage;
  _.changeDeleteHeader_rtelqc$ = changeDeleteHeader;
  _.getConfirmDeleteReturnButton = getConfirmDeleteReturnButton;
  _.getOldDeleteHeader = getOldDeleteHeader;
  _.slidePage_w74nik$ = slidePage;
  _.changePage_midh0e$ = changePage_0;
  _.movePage_h2l30b$ = movePage;
  _.getNewPage_61zpoe$ = getNewPage;
  _.getConfirmDeletePage = getConfirmDeletePage;
  _.getDeletePage = getDeletePage;
  _.getInputForm_8qnmx4$ = getInputForm;
  _.addPageEvent_d3ejew$ = addPageEvent;
  _.confirmDeleteBadEvent_61zpoe$ = confirmDeleteBadEvent;
  _.setLogout = setLogout;
  _.mainColor = mainColor;
  _.secondColor = secondColor;
  _.badColor = badColor;
  _.setRequestEvent_v2ti94$ = setRequestEvent;
  _.jsonifyForm_c879xe$ = jsonifyForm;
  Object.defineProperty(RequestInputs, 'Companion', {get: RequestInputs$Companion_getInstance});
  Object.defineProperty(RequestInputs, '$serializer', {get: RequestInputs$$serializer_getInstance});
  _.RequestInputs_init_1nmmfw$ = RequestInputs_init;
  _.RequestInputs = RequestInputs;
  Object.defineProperty(Response, 'Companion', {get: Response$Companion_getInstance});
  Response.$serializer_init_swdriu$ = Response$Response$$serializer_init;
  Response.$serializer = Response$$serializer;
  _.Response_init_r603xs$ = Response_init;
  _.Response = Response;
  $$importsForInline$$['kotlinx-serialization-kotlinx-serialization-json-jsLegacy'] = $module$kotlinx_serialization_kotlinx_serialization_json_jsLegacy;
  RequestInputs$$serializer.prototype.typeParametersSerializers = GeneratedSerializer.prototype.typeParametersSerializers;
  map = hashMapOf([to('login', map$lambda), to('register', map$lambda_0)]);
  tablesSizes = HashMap_init();
  tableMoving = HashMap_init();
  monthSizes = hashMapOf([to(1, 31), to(2, 28), to(3, 31), to(4, 30), to(5, 31), to(6, 30), to(7, 31), to(8, 31), to(9, 30), to(10, 31), to(11, 30), to(12, 31)]);
  var list = ArrayList_init(12);
  for (var index_0 = 0; index_0 < 12; index_0++) {
    var tmp$ = list.add_11rb$;
    var tmp$_0 = new NormalDate(2021, index_0 + 1 | 0, 1);
    var size = ensureNotNull(monthSizes.get_11rb$(index_0 + 1 | 0));
    var list_0 = ArrayList_init(size);
    for (var index_1 = 0; index_1 < size; index_1++) {
      list_0.add_11rb$(new Day(index_1 + 1 | 0, false));
    }
    tmp$.call(list, new Month(tmp$_0, list_0));
  }
  TABLES = listOf(new Table('Template', list));
  emailForm = new SettingsProperty('email', ['input-email']);
  nameForm = new SettingsProperty('name', ['input-name']);
  passwordForm = new SettingsProperty('password', ['input-password', 'input-repeat-password']);
  deleteForm = new SettingsProperty('delete', ['input-delete'], deleteForm$lambda);
  confirmDeleteForm = new SettingsProperty('confirm-delete', ['input-confirm-delete'], confirmDeleteForm$lambda, confirmDeleteForm$lambda_0);
  promiseMap = hashMapOf([to('name', false), to('email', false), to('password', false), to('repeat-password', false), to('delete', false), to('confirm-delete', false)]);
  var tmp$_1;
  root = Kotlin.isType(tmp$_1 = document.getElementsByTagName('html')[0], HTMLHtmlElement) ? tmp$_1 : throwCCE();
  main();
  return _;
}));

//# sourceMappingURL=MasterCalendar-client.js.map
