{
  mode: 'production',
  resolve: {
    modules: [
      'C:\\Users\\809542\\Desktop\\Kotlin\\MasterCalendar\\build\\js\\packages\\MasterCalendar-client\\kotlin-dce',
      'node_modules'
    ]
  },
  plugins: [
    ProgressPlugin {
      profile: false,
      handler: [Function: handler],
      modulesCount: 500,
      showEntries: false,
      showModules: true,
      showActiveModules: true
    },
    TeamCityErrorPlugin {}
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          'source-map-loader'
        ],
        enforce: 'pre'
      }
    ]
  },
  entry: {
    main: [
      'C:\\Users\\809542\\Desktop\\Kotlin\\MasterCalendar\\build\\js\\packages\\MasterCalendar-client\\kotlin-dce\\MasterCalendar-client.js'
    ]
  },
  output: {
    path: 'C:\\Users\\809542\\Desktop\\Kotlin\\MasterCalendar\\client\\build\\distributions',
    filename: [Function: filename],
    library: 'client',
    libraryTarget: 'umd',
    globalObject: 'this'
  },
  devtool: 'source-map',
  stats: {
    warningsFilter: [
      /Failed to parse source map/
    ],
    warnings: false,
    errors: false
  }
}